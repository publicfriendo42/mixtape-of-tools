#!/bin/bash

# this script is used for the OSX version of Libreoffice, which is organized as a packaged app
# it should not be necessary if you use linux, i'm not sure about windows.
# Originated from https://ask.libreoffice.org/t/how-to-convert-documents-to-pdf-on-osx/3137/3
#
# Libreoffice version: 6.4.2.2
# System: Osx Mojave 10.14.6

# Install instructions
# 
# open terminal
# copy the file to /usr/local/bin
# make it executable typing 
# sudo chmod +x /usr/loca/bin/soffice.sh

# Need to do this because symlink won't work
# It complains about some .plist files
/Applications/LibreOffice.app/Contents/MacOS/soffice "$@"
