# V. 0.1
# 09 december 2021
# Trasforma.sh is a shell comand to convert every ODP file in the path and relative subolders, to pdf.
# Requirements: This cli tool requires "soffice.sh" as command, which is an utilty inside the libreoffice package
# check inside this same repository "soffice.sh"
# System tested: OSX 10.14.6
#
# Usage : run "sh trasforma.sh"
# To improve: all the PDFs are written in the same starting folder and have to be moved inside the relative subfolder. It should be possible to write them directly in the relative folder.

find . -type f -name '*.odp' |
    while IFS= read file_name; do
        echo  "check: $file_name" 
        soffice.sh --headless --convert-to pdf "$file_name"
    done
