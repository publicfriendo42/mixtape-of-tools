# Mixtape Of Tools

I hope to create an interesting repository of utilities, feel free to contribute, everything is cc0 (public domain), feel free to fork! 

# List of utilities

- [Soffice.sh](soffice.sh) : create a link to the soffice command line for OSX, it's necessary to make batch conversion to libreoffice documents.
- [Trasforma.sh](trasforma.sh): batch process to crawl all the files and folders from the starting point and convert all the ODP (presentations) to PDF.

# Who am i ?

My name is Andrea Scarpetta, italian old man. I've been many things: Generation X, Pc technician, Coder, Marketer (SEO), Affiliate. I've been dabbling with windows, unix, osx, linux. 

This repository is my first experiment to spread the utilities that i've been using during the years and maybe help somebody else.
